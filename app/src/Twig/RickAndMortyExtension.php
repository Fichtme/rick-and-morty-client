<?php

namespace App\Twig;

use App\Client\Resources\Collections\EpisodeCollection;
use App\Client\Resources\Collections\LocationCollection;
use App\Client\Resources\Episode;
use App\Client\Resources\Location;
use App\Service\EpisodeService;
use App\Service\LocationService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class DimensionExtension
 * @package App\Twig
 */
class RickAndMortyExtension extends AbstractExtension
{
    /**
     * @var LocationService
     */
    private LocationService $locationService;
    /**
     * @var EpisodeService
     */
    private EpisodeService $episodeService;

    /**
     * RickAndMortyExtension constructor.
     * @param LocationService $locationService
     * @param EpisodeService $episodeService
     */
    public function __construct(
        LocationService $locationService,
        EpisodeService $episodeService
    ) {
        $this->locationService = $locationService;
        $this->episodeService = $episodeService;
    }

    /**
     * @return array|Twig_Function[]
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('getDimensions', [$this, 'getDimensions']),
            new TwigFunction('getLocations', [$this, 'getLocations']),
            new TwigFunction('getEpisodes', [$this, 'getEpisodes']),
        );
    }

    /**
     * @return array
     */
    public function getDimensions()
    {
        $locations = $this->locationService->findByProperties();

        foreach ($locations as $location) {
            $dimensions[] = $location->dimension;
        }

        return array_unique($dimensions);
    }

    /**
     * @return EpisodeCollection|Episode[]
     */
    public function getEpisodes()
    {
        return $this->episodeService->findByProperties();
    }

    /**
     * @return LocationCollection|Location[]
     */
    public function getLocations()
    {
        return $this->locationService->findByProperties();
    }
}
