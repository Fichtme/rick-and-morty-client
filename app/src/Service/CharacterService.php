<?php

namespace App\Service;

use App\Client\Resources\Character;
use App\Client\Resources\Collections\CharacterCollection;
use App\Client\Resources\Location;
use App\Client\RickAndMortyClient;

/**
 * Class CharacterService
 * @package App\Service
 */
class CharacterService
{
    /**
     * @var RickAndMortyClient
     */
    private RickAndMortyClient $client;

    public function __construct(RickAndMortyClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param array $properties
     * @return Character[]|CharacterCollection
     */
    public function findByProperties(array $properties = []): CharacterCollection
    {
        if ($properties['dimension'] ?? false) {
            return $this->findByDimension($properties['dimension']);
        }

        if ($properties['episode'] ?? false) {
            return $this->findByEpisode($properties['episode']);
        }

        if ($properties['location'] ?? false) {
            return $this->findByLocation($properties['location']);
        }

        // fetch all characters
        return $this->client->character->list();
    }

    /**
     * @param string $dimension
     * @return Character[]|CharacterCollection
     */
    private function findByDimension(string $dimension): CharacterCollection
    {
        // fetch locations from given dimension
        $locations = $this->client->location->list(['dimension' => $dimension]);
        $characterIds = [];
        /** @var Location $location */
        foreach ($locations as $location) {
            $characterIds = array_merge($characterIds, $location->residents);
        }

        // fetch only related characters
        return $this->client->character->getByIds($characterIds);
    }

    /**
     * @param int $episode
     * @return CharacterCollection
     */
    private function findByEpisode(int $episodeId): CharacterCollection
    {
        // fetch episodes from given episode
        $episodes = $this->client->episode->get($episodeId);
        $characterIds = [];
        foreach ($episodes as $episode) {
            $characterIds = array_merge($characterIds, $episode->characters);
        }

        // fetch only related characters
        return $this->client->character->getByIds($characterIds);
    }

    /**
     * @param int $locationId
     * @return CharacterCollection
     */
    private function findByLocation(int $locationId): CharacterCollection
    {
        // fetch location from given location
        $locations = $this->client->location->get($locationId);
        $characterIds = [];
        /** @var Location $location */
        foreach ($locations as $location) {
            $characterIds = array_merge($characterIds, $location->residents);
        }

        // fetch only related characters
        return $this->client->character->getByIds($characterIds);
    }
}
