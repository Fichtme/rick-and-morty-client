<?php

namespace App\Service;

use App\Client\Resources\Collections\EpisodeCollection;
use App\Client\Resources\Episode;
use App\Client\RickAndMortyClient;

/**
 * Class CharacterService
 * @package App\Service
 */
class EpisodeService
{
    /**
     * @var RickAndMortyClient
     */
    private RickAndMortyClient $client;

    public function __construct(RickAndMortyClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param array $properties
     * @return Episode[]|EpisodeCollection
     */
    public function findByProperties(array $properties = []): EpisodeCollection
    {
        return $this->client->episode->list();
    }
}
