<?php

namespace App\Service;

use App\Client\Resources\Collections\LocationCollection;
use App\Client\Resources\Location;
use App\Client\RickAndMortyClient;

/**
 * Class CharacterService
 * @package App\Service
 */
class LocationService
{
    /**
     * @var RickAndMortyClient
     */
    private RickAndMortyClient $client;

    public function __construct(RickAndMortyClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param array $properties
     * @return Location[]|LocationCollection
     */
    public function findByProperties(array $properties = []): LocationCollection
    {
        return $this->client->location->list();
    }
}
