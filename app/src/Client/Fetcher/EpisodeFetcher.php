<?php

namespace App\Client\Fetcher;

use App\Client\Resources\Character;
use App\Client\Resources\Collections\CharacterCollection;
use App\Client\Resources\Collections\CollectionInterface;
use App\Client\Resources\Collections\EpisodeCollection;

/**
 * Class CharacterFetcher
 * @package App\Client\Fetcher
 */
class EpisodeFetcher extends AbstractFetcher
{
    /**
     * @return CharacterCollection
     */
    public function getResourceCollectionObject(): CollectionInterface
    {
        return new EpisodeCollection();
    }

    public function getResourcePath(): string
    {
        return 'episode';
    }
}
