<?php

namespace App\Client\Fetcher;

use App\Client\Resources\Character;
use App\Client\Resources\Collections\CharacterCollection;
use App\Client\Resources\Collections\CollectionInterface;

/**
 * Class CharacterFetcher
 * @package App\Client\Fetcher
 */
class CharacterFetcher extends AbstractFetcher
{
    /**
     * @return CharacterCollection
     */
    public function getResourceCollectionObject(): CollectionInterface
    {
        return new CharacterCollection();
    }

    public function getResourcePath(): string
    {
        return 'character';
    }
}
