<?php

namespace App\Client\Fetcher;

use App\Client\Resources\Character;
use App\Client\Resources\Collections\CharacterCollection;
use App\Client\Resources\Collections\CollectionInterface;
use App\Client\Resources\Collections\EpisodeCollection;
use App\Client\Resources\Collections\LocationCollection;

/**
 * Class CharacterFetcher
 * @package App\Client\Fetcher
 */
class LocationFetcher extends AbstractFetcher
{
    /**
     * @return CharacterCollection
     */
    public function getResourceCollectionObject(): CollectionInterface
    {
        return new LocationCollection();
    }

    public function getResourcePath(): string
    {
        return 'location';
    }
}
