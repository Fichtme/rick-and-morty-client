<?php

namespace App\Client\Fetcher;

use App\Client\Resources\AbstractResource;
use App\Client\Resources\Collections\AbstractCollection;
use App\Client\Resources\Collections\CollectionInterface;
use App\Client\Resources\ResourceInterface;
use App\Client\RickAndMortyClient;
use App\Exception\MapperException;
use ReflectionClass;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

/**
 * Class AbstractFetcher
 * @package App\Client\Fetcher
 */
abstract class AbstractFetcher implements FetcherInterface
{
    /**
     * @var RickAndMortyClient
     */
    protected RickAndMortyClient $client;

    /**
     * AbstractFetcher constructor.
     * @param RickAndMortyClient $client
     */
    public function __construct(RickAndMortyClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param array $ids
     * @return CollectionInterface
     */
    public function getByIds(array $ids): CollectionInterface
    {
        $stringIds = implode(',', $ids);
        $results = $this->client->read($this->getResourcePath().'/'.$stringIds);
        // check if result is not a single result
        if (isset($results['id'])) {
            $results = [$results];
        }

        $collection = clone $this->getResourceCollectionObject();
        foreach ($results as $key => $r) {
            $resource = $this->map($r);
            if ($resource) {
                $collection->append($resource);
            }
        }

        return $collection;
    }

    /**
     * @param $id
     * @return CollectionInterface
     */
    public function get($id): CollectionInterface
    {
        $result = $this->client->read($this->getResourcePath().'/'.$id);
        $collection = clone $this->getResourceCollectionObject();
        $resource = $this->map($result);
        if ($resource) {
            $collection->append($resource);
        }

        return $collection;
    }

    /**
     * @param array $arguments
     * @return CollectionInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function list(array $arguments = []): CollectionInterface
    {
        $apiPath = $this->getResourcePath();

        $arguments = array_filter($arguments);
        if (count($arguments)) {
            $apiPath .= $this->buildQueryString($arguments);
        }

        $results = $this->client->list($apiPath)['results'];

        $collection = clone $this->getResourceCollectionObject();
        foreach ($results as $key => $r) {
            $resource = $this->map($r);
            if ($resource) {
                $collection->append($resource);
            }
        }

        return $collection;
    }

    /**
     * @return ResourceInterface
     */
    private function getObject(): ResourceInterface
    {
        return $this
            ->getResourceCollectionObject()
            ->getResourceObject();
    }

    /**
     * @return CollectionInterface
     */
    abstract public function getResourceCollectionObject(): CollectionInterface;

    /**
     * @return string
     */
    abstract public function getResourcePath(): string;

    /**
     * @param array $objectData
     * @param ResourceInterface|null $resource
     * @return ResourceInterface
     */
    private function map(array $objectData, ResourceInterface $resource = null): ?ResourceInterface
    {
        if ($resource) {
            $object = clone $resource;
        } else {
            $object = clone $this->getObject();
        }

        if (!isset($objectData['id'])) {
            return null;
        }

        foreach ($objectData as $field => $value) {
            if (is_array($value) && !isset($value['url'])) {
                $ids = [];
                foreach ($value as $url) {
                    $ids[] = substr($url, strrpos($url, '/') + 1);
                }

                $object->$field = $ids;
            } else {
                $object->$field = $value;
            }
        }

        return $object;
    }

    private function buildQueryString(array $properties): string
    {
        if (empty($properties)) {
            return '';
        }

        return '?' . http_build_query($properties, '', '&');
    }
}
