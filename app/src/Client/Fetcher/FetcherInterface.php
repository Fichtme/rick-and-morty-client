<?php

namespace App\Client\Fetcher;

use App\Client\Resources\Collections\CollectionInterface;

/**
 * Class AbstractFetcher
 * @package App\Client\Fetcher
 */
interface FetcherInterface
{
    /**
     * @return CollectionInterface
     */
    public function getResourceCollectionObject(): CollectionInterface;

    /**
     * @return string
     */
    public function getResourcePath(): string;
}
