<?php

namespace App\Client;

use App\Client\Fetcher\CharacterFetcher;
use App\Client\Fetcher\EpisodeFetcher;
use App\Client\Fetcher\LocationFetcher;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;

/**
 * Class RickAndMortyClient
 * @package App\Client
 */
class RickAndMortyClient
{
    public const BASE_URL = "https://rickandmortyapi.com/api/";
    public const DEFAULT_USER_AGENT = "rickandmorty/client language/php7.4";
    public const CLIENT_VERSION = '1.0.0';
    private string $userAgent;

    /**
     * @var CharacterFetcher
     */
    public CharacterFetcher $character;
    /**
     * @var EpisodeFetcher
     */
    public EpisodeFetcher $episode;
    /**
     * @var LocationFetcher
     */
    public LocationFetcher $location;

    /**
     * @var Client
     */
    private Client $client;

    /**
     * RickAndMortyClient constructor.
     * @param string $userAgent
     */
    public function __construct(string $userAgent = self::DEFAULT_USER_AGENT)
    {
        $this->initialize();

        $this->client = new Client([
            'base_uri' => self::BASE_URL,
            RequestOptions::TIMEOUT => 5,
            RequestOptions::READ_TIMEOUT => 5,
            RequestOptions::HEADERS => [
                'User-Agent' => $userAgent,
                'Client-Version' => self::CLIENT_VERSION,
            ],
        ]);
    }

    /**
     * Initialize objects
     */
    private function initialize()
    {
        $this->character = new CharacterFetcher($this);
        $this->episode = new EpisodeFetcher($this);
        $this->location = new LocationFetcher($this);
    }

    /**
     * @param string $url
     *
     * @return array
     */
    public function read(string $url): array
    {
        return $this->sendRequest($url);
    }

    /**
     * @param $url
     * @param string $method
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    private function sendRequest($url, $method = 'get'): array
    {
        $request = new Request($method, $url);

        $response = $this->client->send($request, ['http_errors' => false]);

        return json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @param string $url
     * @param array $prevResults
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function list(string $url, array $prevResults = []): array
    {
        $data = $this->sendRequest($url);
        $results = array_merge($prevResults, $data['results'] ?? []);
        $next = $data['info']['next'] ?? null;
        if ($next) {
            return $this->list($next, $results);
        }
        $data['results'] = $results;
        return $data;
    }
}
