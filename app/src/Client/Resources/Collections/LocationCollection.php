<?php

namespace App\Client\Resources\Collections;

use App\Client\Resources\Location;
use App\Client\Resources\ResourceInterface;

/**
 * Class CharacterCollection
 * @package App\Client\Resources\Collections
 */
class LocationCollection extends AbstractCollection
{
    /**
     * @return string
     */
    public function getCollectionResourceName(): string
    {
        return 'locations';
    }

    /**
     * @return ResourceInterface
     */
    public function getResourceObject(): ResourceInterface
    {
        return new Location();
    }
}
