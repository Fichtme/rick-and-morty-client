<?php

namespace App\Client\Resources\Collections;

use App\Client\Resources\Character;
use App\Client\Resources\ResourceInterface;

/**
 * Class CharacterCollection
 * @package App\Client\Resources\Collections
 */
class CharacterCollection extends AbstractCollection
{
    /**
     * @return string
     */
    public function getCollectionResourceName(): string
    {
        return 'characters';
    }

    /**
     * @return ResourceInterface
     */
    public function getResourceObject(): ResourceInterface
    {
        return new Character();
    }
}
