<?php

namespace App\Client\Resources\Collections;

use App\Client\Resources\ResourceInterface;
use ArrayObject;

/**
 * Class AbstractCollection
 * @package App\Client\Resources\Collections
 */
abstract class AbstractCollection extends ArrayObject implements CollectionInterface
{
    /**
     * @return ResourceInterface
     */
    abstract public function getResourceObject(): ResourceInterface;

    /**
     * @return string
     */
    abstract public function getCollectionResourceName(): string;
}
