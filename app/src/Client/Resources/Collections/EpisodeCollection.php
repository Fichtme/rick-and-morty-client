<?php

namespace App\Client\Resources\Collections;

use App\Client\Resources\Episode;
use App\Client\Resources\ResourceInterface;

/**
 * Class CharacterCollection
 * @package App\Client\Resources\Collections
 */
class EpisodeCollection extends AbstractCollection
{
    /**
     * @return string
     */
    public function getCollectionResourceName(): string
    {
        return 'episodes';
    }

    /**
     * @return ResourceInterface
     */
    public function getResourceObject(): ResourceInterface
    {
        return new Episode();
    }
}
