<?php

namespace App\Client\Resources\Collections;

use App\Client\Resources\ResourceInterface;

/**
 * Class AbstractCollection
 * @package App\Client\Resources\Collections
 */
interface CollectionInterface
{
    /**
     * @return ResourceInterface
     */
    public function getResourceObject(): ResourceInterface;

    /**
     * @return string
     */
    public function getCollectionResourceName(): string;

    /**
     * @param $value
     */
    public function append($value);
}
