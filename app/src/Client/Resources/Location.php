<?php

namespace App\Client\Resources;

use App\Client\Resources\Collections\CharacterCollection;

/**
 * Class Location
 * @package App\Client\Resources
 */
class Location extends AbstractResource
{
    public string $id;
    public string $name;
    public string $type;
    public string $dimension;
    public array $residents;
    public string $created;
}
