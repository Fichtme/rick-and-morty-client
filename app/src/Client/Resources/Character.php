<?php

namespace App\Client\Resources;

use App\Client\Resources\Collections\EpisodeCollection;

/**
 * Class Character
 * @package App\Client\Resources
 */
class Character extends AbstractResource
{
    public string $id;
    public string $name;
    public string $status;
    public string $species;
    public string $type;
    public string $gender;
    public array $origin;
    public array $location;
    public string $image;
    public array $episode;
    public string $created;
}
