<?php

namespace App\Client\Resources;

/**
 * Class AbstractResource
 * @package App\Client\Resources
 */
abstract class AbstractResource implements ResourceInterface
{
    public function getId()
    {
        return $this->id;
    }
}
