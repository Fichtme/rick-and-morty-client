<?php

namespace App\Client\Resources;

/**
 * Class Episode
 * @package App\Client\Resources
 */
class Episode extends AbstractResource
{
    public string $id;
    public string $name;
    public string $airDate;
    public string $episode;
    public array $characters;
    public string $created;
}
