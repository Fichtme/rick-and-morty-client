<?php

namespace App\Controller;

use App\Service\CharacterService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CharacterController extends AbstractController
{
    /**
     * @var CharacterService
     */
    private CharacterService $characterService;

    public function __construct(CharacterService $characterService)
    {
        $this->characterService = $characterService;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function indexView(Request $request): Response
    {
        $characters = $this->characterService->findByProperties([
            'character' => $request->get('character'),
            'dimension' => $request->get('dimension'),
            'episode' => $request->get('episode'),
            'location' => $request->get('location'),
        ]);

        return $this->render("character-list.html.twig", [
            "characters" => $characters
        ]);
    }
}
