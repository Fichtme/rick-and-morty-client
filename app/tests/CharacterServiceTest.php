<?php

namespace App\Tests;

use App\Service\CharacterService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Some really basic tests, need to mock will break in the future...
 *
 * Class CharacterServiceTest
 * @package App\Tests
 */
class CharacterServiceTest extends KernelTestCase
{
    public function setUp(): void
    {
        self::bootKernel();
    }

    public function testFetchCharactersThatExistInAGivenDimension()
    {
        /** @var CharacterService $characterService */
        $characterService = self::$container->get(CharacterService::class);
        $results = $characterService->findByProperties(['dimension' => 'Fantasy Dimension']);

        self::assertCount(7, $results);
    }

    public function testFetchCharactersThatExistInAGivenLocation()
    {
        /** @var CharacterService $characterService */
        $characterService = self::$container->get(CharacterService::class);
        $results = $characterService->findByProperties(['location' => 1]);

        self::assertCount(27, $results);
    }

    public function testFetchCharactersThatPartakeInAGivenEpisode()
    {
        /** @var CharacterService $characterService */
        $characterService = self::$container->get(CharacterService::class);
        $results = $characterService->findByProperties(['episode' => '1']);

        self::assertCount(19, $results);
    }
}