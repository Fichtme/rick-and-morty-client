<?php

namespace App\Tests;

use App\Service\CharacterService;
use App\Service\EpisodeService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Some really basic tests, need to mock will break in the future...
 *
 * Class EpisodeServiceTest
 * @package App\Tests
 */
class EpisodeServiceTest extends KernelTestCase
{
    public function setUp(): void
    {
        self::bootKernel();
    }

    public function testFetchEpisodes()
    {
        /** @var EpisodeService $service */
        $service = self::$container->get(EpisodeService::class);
        $results = $service->findByProperties();

        self::assertCount(36, $results);
    }
}