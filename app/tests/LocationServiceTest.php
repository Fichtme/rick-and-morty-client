<?php

namespace App\Tests;

use App\Client\Resources\Location;
use App\Service\CharacterService;
use App\Service\EpisodeService;
use App\Service\LocationService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Some really basic tests, need to mock will break in the future...
 *
 * Class LocationServiceTest
 * @package App\Tests
 */
class LocationServiceTest extends KernelTestCase
{
    public function setUp(): void
    {
        self::bootKernel();
    }

    public function testFetchLocations()
    {
        /** @var LocationService $service */
        $service = self::$container->get(LocationService::class);
        $results = $service->findByProperties();

        self::assertCount(94, $results);
    }
}