# Rick and Morty
A fun way to write a basic api client

## What?
It comes down to getting data from an API and display it using only **Symfony**. You can find the API here: https://rickandmortyapi.com/
   
Get the following data from the API
1. Show all Characters that exist (or are last seen) in a given Dimension;
1. Show all Characters that exist (or are last seen) at a given Location;
1. Show all Characters that partake in a given Episode;
1. Showing all information of a Character (Name, Species, Gender, Location, Dimension, etc).

## local installation
Duplicate & configure your .env file
```console
cp .env.dist .env
```

Build Docker
```bash
docker-compose build
```

Start Docker
```bash
docker-compose up
```

Wait for it to start up and visit http://localhost:8080
